"use strict";

const bcrypt = require('bcryptjs');

module.exports = function (sequelize, DataTypes) {
    return sequelize.define("available_tables", {
        restaurantID: DataTypes.INTEGER,
        reservationDate: DataTypes.DATE,
        twoSeatTables: DataTypes.INTEGER,
        fourSeatTables: DataTypes.INTEGER,
        sixSeatTables: DataTypes.INTEGER
    });
};
