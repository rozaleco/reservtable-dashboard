"use strict";

const bcrypt = require('bcryptjs');

module.exports = function (sequelize, DataTypes) {
    return sequelize.define("table", {
        restaurantName: DataTypes.STRING,
        restaurantAddress: DataTypes.STRING,
        restaurantCity: DataTypes.STRING,
        restaurantType: DataTypes.STRING,
        restaurantDescription: DataTypes.STRING,
        email: {type: DataTypes.STRING, unique: true},
        phoneNumber: DataTypes.STRING,
    });
};
