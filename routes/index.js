const express = require('express');
const app = express();
var path = require('path');
const Sequelize = require("sequelize");
const Op = Sequelize.Op;
const form = require('express-form'),
  field = form.field;
const _ = require('underscore'); //lodash
// const nodemailer = require('nodemailer'); // Mailservice

if (typeof localStorage === "undefined" || localStorage === null) {
  var LocalStorage = require('node-localstorage').LocalStorage;
  localStorage = new LocalStorage('./scratch');
}

const randomString = require('randomstring'); //random string generator
const multer = require('multer'); //image uploading

const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, 'public/uploads/')
  },
  filename: function(req, file, cb) {
    var string = randomString.generate({
      length: 5,
      charset: 'alphabetic'
    });
    var filetype = file.originalname.split('.').pop();
    cb(null, Date.now() + string + '.' + filetype);
  },
})
const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1000000, //kb image size
    files: 1
  },
});

const models = require('../models/index');

app.get('/', function(req, res, next) {
  res.redirect('/dashboard');
});

app.get('/dashboard', function(req, res) {
  res.render('dashboard', {
    validations: null
  });
});

app.get('/update-restaurant/:id', function(req, res) {
  models.restaurant.findById(req.params.id).then((restaurant) => {
    res.render('update-restaurant', {
      restaurant: restaurant,
      validations: null,
    });
  })
});

app.post('/update-restaurant/:id',
  upload.single('restaurantImage'),
  form(
    field("restaurantName").trim().required("", "Restaurant Name is required."),
    field("restaurantAddress").trim().required("", "Restaurant Address is required."),
    field("restaurantCity").trim().required("", "Restaurant City is required."),
    field("restaurantType").trim().required("", "Restaurant Type is required."),
    field("email").trim().required("", "Email cannot be blank. ")
    .is(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/, "Please enter a valid email address."),
    field("phoneNumber").trim().required("", "Phone Number is required."),
    field("openingHrs").trim().required("", "Opening Hours is required."),
    field("twoSeatTables").trim().required("", "This field is required."),
    field("fourSeatTables").trim().required("", "This field is required."),
    field("sixSeatTables").trim().required("", "This field is required."),
  ),
  function(req, res) {
    if (!req.form.isValid) {
      console.log('Invalid');
      models.restaurant.findById(req.params.id).then(restaurant => {
        res.render('update-restaurant', {
          restaurant: restaurant,
          form: req.form,
          errors: null,
        });
      });
    } else {
      for (const field in req.body) {
        if (req.body[field] === '') {
          delete req.body[field]
        }
      }
      const body = req.body;
      if (req.file) {
        body['restaurantImage'] = 'uploads/' + req.file.filename;
      }
      body["twoSeatTables"] = parseInt(body["twoSeatTables"], 10);
      body["fourSeatTables"] = parseInt(body["fourSeatTables"], 10);
      body["sixSeatTables"] = parseInt(body["sixSeatTables"], 10);
      delete req.body.tableCount;
      models.restaurant.update(body, {
        where: {
          id: req.params.id
        }
      }).then(restaurant => {
        res.redirect('/restaurant-list');
      }).catch(e => {
        models.restaurant.findById(req.params.id).then((restaurant) => {
          if (e.name === 'SequelizeUniqueConstraintError')
            res.render('update-restaurant', {
              restaurant: restaurant,
              form: req.form,
              categories: req.session.categories,
              validations: [{
                message: "Email already exist."
              }]
            });
          else
            res.render('update-restaurant', {
              restaurant: restaurant,
              form: req.form,
              validations: e.errors
            });
        })
      });
    }
  });

app.get('/view-restaurant/:id', function(req, res) {
  models.restaurant.findById(req.params.id).then((restaurant) => {
    res.render('view-restaurant', {
      restaurant: restaurant
    });
  });
})

app.get('/restaurant-list', function(req, res) {
  models.restaurant.findAll().then((restaurants) => {
    res.render('restaurant-list', {
      restaurants: restaurants
    });
  })
});

app.get('/reservation-list', function(req, res) {
  models.reservations.findAll().then((reservations) => {
    res.render('reservation-list', {
      reservations: reservations
    });
  })
});

app.delete('/reservation/:id', function(req, res) {
  models.reservations.findById(req.params.id).then((reservation) => {
    reservation.destroy();
    res.status(200).send({
      message: 'Reservation Deleted.'
    });
    setTimeout(() => {
      res.redirect('/reservation-list');
    }, 3000)
  }, (e) => {
    res.status(400).send(e);
  })
})

app.get('/view-reservation/:id', function(req, res) {
  models.reservations.findById(req.params.id).then((reservation) => {
    res.render('view-reservation', {
      reservation: reservation
    });
  });
});

app.get('/contact', function(req, res) {
  res.render('contact', {
    validations: null
  });
});

app.get('/reservationList', function(req, res) {
  models.reservation.findAll().then(reservations => {
    res.send('reservations', {
      reservations: reservations
    });
  })
});

app.get('/reserve-table/:id', function(req, res) {
  models.restaurant.findById(req.params.id).then(restaurant => {
    localStorage.setItem('tableCount', JSON.stringify({
      'twoSeatTables': restaurant.dataValues.twoSeatTables,
      'fourSeatTables': restaurant.dataValues.fourSeatTables,
      'sixSeatTables': restaurant.dataValues.sixSeatTables,
    }))
    localStorage.setItem('restaurantName', restaurant.dataValues.restaurantName)
    if (restaurant) {
      res.render('reservation-date', {
        restaurant: restaurant
      });
    }
  })
});

app.post('/reserve-table/:id',
  form(
    field("reservationDate").trim().required("", "Reservation date is required."),
    field("time").trim().required("", "Reservation time is required."),
    field("attendCount").trim().required("", "Number of guests are required."),
  ),
  function(req, res, next) {
    if (!req.form.isValid) {
      models.restaurant.findById(req.params.id).then(restaurant => {
        res.render('reservation-date', {
          restaurant: restaurant,
          form: req.form,
          errors: null,
          categories: req.session.categories
        });
      });
    } else {
      const today = new Date();
      const inputToDate = Date.parse(req.form.reservationDate);
      const compareDate = Date.parse(today);
      if (inputToDate < compareDate) {
        models.restaurant.findById(req.params.id).then(restaurant => {
          res.render('reservation-date', {
            restaurant: restaurant,
            form: req.form,
            categories: req.session.categories,
            validations: [{
              message: "Date is invalid"
            }]
          });
        });
      } else {
        req.body['reservationDate'] = req.body.reservationDate + ' ' + req.body.time + 'Z'
        delete req.body.time
        delete req.body.date
        models.available_tables.find({
          where: {
            reservationDate: req.body.reservationDate
          }
        }).then(table => {
          if (req.body.attendCount < 3) {
            if (table) {
              localStorage.setItem('tableCount', JSON.stringify({
                'twoSeatTables': table.dataValues.twoSeatTables,
                'fourSeatTables': table.dataValues.fourSeatTables,
                'sixSeatTables': table.dataValues.sixSeatTables,
              }))
              if (table.dataValues.twoSeatTables === 0) {
                models.restaurant.findById(req.params.id).then(restaurant => {
                  localStorage.setItem('tableCount', JSON.stringify({
                    'twoSeatTables': restaurant.dataValues.twoSeatTables,
                    'fourSeatTables': restaurant.dataValues.fourSeatTables,
                    'sixSeatTables': restaurant.dataValues.sixSeatTables,
                  }))
                  res.render('reservation-date', {
                    restaurant: restaurant,
                    form: req.form,
                    categories: req.session.categories,
                    validations: [{
                      message: "Sorry there are no tables available at this time. Please try another date."
                    }]
                  });
                });
              } else {
                const tableCount = JSON.parse(localStorage.getItem('tableCount'))
                tableCount['twoSeatTables'] -= 1;
                localStorage.setItem('tableCount', JSON.stringify(tableCount));
              }
            } else {
              const tableCount = JSON.parse(localStorage.getItem('tableCount'))
              tableCount['twoSeatTables'] -= 1;
              localStorage.setItem('tableCount', JSON.stringify(tableCount));
            }
          } else if (req.body.attendCount < 6) {
            if (table) {
              localStorage.setItem('tableCount', JSON.stringify({
                'twoSeatTables': table.dataValues.twoSeatTables,
                'fourSeatTables': table.dataValues.fourSeatTables,
                'sixSeatTables': table.dataValues.sixSeatTables,
              }))
              if (table.dataValues.fourSeatTables === 0) {
                localStorage.setItem('tableCount', JSON.stringify({
                  'twoSeatTables': table.dataValues.twoSeatTables,
                  'fourSeatTables': table.dataValues.fourSeatTables,
                  'sixSeatTables': table.dataValues.sixSeatTables,
                }))
                models.restaurant.findById(req.params.id).then(restaurant => {
                  localStorage.setItem('tableCount', JSON.stringify({
                    'twoSeatTables': restaurant.dataValues.twoSeatTables,
                    'fourSeatTables': restaurant.dataValues.fourSeatTables,
                    'sixSeatTables': restaurant.dataValues.sixSeatTables,
                  }))
                  res.render('reservation-date', {
                    restaurant: restaurant,
                    form: req.form,
                    categories: req.session.categories,
                    validations: [{
                      message: "Sorry there are no tables available at this time. Please try another date."
                    }]
                  });
                });
              } else {
                const tableCount = JSON.parse(localStorage.getItem('tableCount'))
                tableCount['fourSeatTables'] -= 1;
                localStorage.setItem('tableCount', JSON.stringify(tableCount));
              }
            } else {
              const tableCount = JSON.parse(localStorage.getItem('tableCount'))
              tableCount['fourSeatTables'] -= 1;
              localStorage.setItem('tableCount', JSON.stringify(tableCount));
            }
          } else if (req.body.attendCount > 5) {
            if (table) {
              localStorage.setItem('tableCount', JSON.stringify({
                'twoSeatTables': table.dataValues.twoSeatTables,
                'fourSeatTables': table.dataValues.fourSeatTables,
                'sixSeatTables': table.dataValues.sixSeatTables,
              }))
              if (table.dataValues.sixSeatTables === 0) {
                localStorage.setItem('tableCount', JSON.stringify({
                  'twoSeatTables': table.dataValues.twoSeatTables,
                  'fourSeatTables': table.dataValues.fourSeatTables,
                  'sixSeatTables': table.dataValues.sixSeatTables,
                }))
                models.restaurant.findById(req.params.id).then(restaurant => {
                  localStorage.setItem('tableCount', JSON.stringify({
                    'twoSeatTables': restaurant.dataValues.twoSeatTables,
                    'fourSeatTables': restaurant.dataValues.fourSeatTables,
                    'sixSeatTables': restaurant.dataValues.sixSeatTables,
                  }))
                  res.render('reservation-date', {
                    restaurant: restaurant,
                    form: req.form,
                    categories: req.session.categories,
                    validations: [{
                      message: "Sorry there are no tables available at this time. Please try another date."
                    }]
                  });
                });
              } else {
                const tableCount = JSON.parse(localStorage.getItem('tableCount'))
                tableCount['sixSeatTables'] -= 1;
                localStorage.setItem('tableCount', JSON.stringify(tableCount));
              }
            } else {
              const tableCount = JSON.parse(localStorage.getItem('tableCount'))
              tableCount['sixSeatTables'] -= 1;
              localStorage.setItem('tableCount', JSON.stringify(tableCount));
            }
          }
          req.body['restaurantID'] = req.params.id
          req.body = {
            ...JSON.parse(localStorage.getItem('tableCount')),
            ...req.body
          }
          localStorage.setItem('reservationDate', JSON.stringify(req.body))
          models.restaurant.findById(req.params.id).then(restaurant => {
            res.render('reservation', {
              restaurant: restaurant
            });
          })
        });
      }
    }
  });


app.get('/reservation-details/:id', function(req, res) {
  models.restaurant.findById(req.params.id).then(restaurant => {
    res.render('reservation', {
      restaurant: restaurant
    });
  })
});

app.post('/reservation/:id',
  form(
    field("firstName").trim().required("", "First Name is required."),
    field("lastName").trim().required("", "Last Name is required."),
    field("phoneNumber").trim().required("", "Phone Number is required."),
    field("email").trim().required("", "Email is required."),
  ),
  function(req, res) {
    if (!req.form.isValid) {
      models.restaurant.findById(req.params.id).then(restaurant => {
        res.render('reservation', {
          restaurant: restaurant,
          form: req.form,
          errors: null,
          categories: req.session.categories
        });
      });
    } else {
      req.body['status'] = 'pending';
      const body = {
        ...req.body,
        ...JSON.parse(localStorage.getItem('reservationDate'))
      }
      models.available_tables.findOne({
        where: {
          reservationDate: body.reservationDate
        }
      }).then((table) => {
        if (table) {
          table.update(JSON.parse(localStorage.getItem('reservationDate')))
        } else {
          models.available_tables.create(JSON.parse(localStorage.getItem('reservationDate')))
        }
      })
      const transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          user: 'reservetableinfo@gmail.com',
          pass: 'reservetable123'
        }
      })
      const mailOptions = {
        from: 'reservetableinfo@gmail.com',
        to: body.email,
        subject: `New Reservation at ${localStorage.getItem('restaurantName')}`,
        text: `You have successfully made a reservation for ${body.attendCount} on ${body.reservationDate.split(' ')[0]} at ${body.reservationDate.split(' ')[1].split('Z')[0]} in ${localStorage.getItem('restaurantName')}.

      We hope to enjoy your presence.

      Regards,
      The Management`,
        replyTo: 'reservetableinfo@gmail.com'
      }
      transporter.sendMail(mailOptions, function(err, res) {
        if (err) {
          console.error('there was an error: ', err);
        } else {
          console.log('here is the res: ', res)
        }
      })
      models.reservations.create(body).then(reservation => {
        console.log(reservation);
        res.redirect('/home');
      }).catch(e => {
        res.render('reservation', {
          form: req.form,
          categories: req.session.categories,
          validations: e.errors
        });
      });
    }
  });

app.post('/search', form(
    field("searchBox").trim().required("", "Please enter a search term."),
  ),
  function(req, res) {
    console.log(req);
    models.restaurant.findAll({
      where: {
        [Op.or]: [{
            restaurantName: {
              like: '%' + req.body.searchBox + '%'
            }
          },
          {
            restaurantCity: {
              like: '%' + req.body.searchBox + '%'
            }
          },
          {
            restaurantType: {
              like: '%' + req.body.searchBox + '%'
            }
          }
        ]
      }
    }).then((results) => {
      console.log(results);
      if (results.length !== 0) {
        res.render('searchList', {
          searchResults: results,
        })
      } else {
        res.render('searchList', {
          searchResults: undefined
        })
      }
    })
  });

app.get('/register-restaurant', function(req, res) {
  res.render('register-restaurant', {
    validations: null
  });
});

app.post('/register-restaurant',
  upload.single('restaurantImage'),
  form(
    field("restaurantName").trim().required("", "Restaurant Name is required."),
    field("restaurantAddress").trim().required("", "Restaurant Address is required."),
    field("restaurantCity").trim().required("", "Restaurant City is required."),
    field("restaurantType").trim().required("", "Restaurant Type is required."),
    field("restaurantDescription").trim(),
    field("email").trim().required("", "Email cannot be blank. ")
    .is(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/, "Please enter a valid email address."),
    field("phoneNumber").trim().required("", "Phone Number is required."),
    field("openingHrs").trim().required("", "Opening Hours is required."),
    field("twoSeatTables").trim().required("", "This field is required."),
    field("fourSeatTables").trim().required("", "This field is required."),
    field("sixSeatTables").trim().required("", "This field is required."),
  ),
  function(req, res) {
    if (!req.form.isValid) {
      res.render('register-restaurant', {
        form: req.form,
        errors: null,
        categories: req.session.categories
      });
    } else {
      console.log(req.body);
      const body = req.body;
      if (req.file) {
        body['restaurantImage'] = 'uploads/' + req.file.filename;
      }
      body["twoSeatTables"] = parseInt(body["twoSeatTables"], 10);
      body["fourSeatTables"] = parseInt(body["fourSeatTables"], 10);
      body["sixSeatTables"] = parseInt(body["sixSeatTables"], 10);
      delete req.body.tableCount;
      console.log(body);
      const transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          user: 'reservetableinfo@gmail.com',
          pass: 'reservetable123'
        }
      })
      const mailOptions = {
        from: 'reservetableinfo@gmail.com',
        to: body.email,
        subject: `New Restaurant at Reservetable`,
        text: `New Restaurant Information:

Restaurant Name:    ${body.restaurantName}
Address:    ${body.restaurantAddress}
City:   ${body.restaurantCity}
Cuisine Type:   ${body.restaurantType}
Description:    ${body.restaurantDescription}
Contact No:   ${body.phoneNumber}
Opening Hours:    ${body.openingHrs}
Two Seat Tables:    ${body.twoSeatTables}        Four Seat Tables:    ${body.fourSeatTables}        Six Seat Tables:    ${body.sixSeatTables}

We are looking forward to doing business with you.

Regards,
The Management`,
        replyTo: 'reservetableinfo@gmail.com'
      }
      transporter.sendMail(mailOptions, function(err, res) {
        if (err) {
          console.error('there was an error: ', err);
        } else {
          console.log('here is the res: ', res)
        }
      })
      models.restaurant.create(body).then(restaurant => {
        res.redirect('/home');
      }).catch(e => {
        if (e.name === 'SequelizeUniqueConstraintError')
          res.render('register-restaurant', {
            form: req.form,
            categories: req.session.categories,
            validations: [{
              message: "Email already exist."
            }]
          });
        else
          res.render('register-restaurant', {
            form: req.form,
            categories: req.session.categories,
            validations: e.errors
          });
      });
    }
  });

module.exports = app;
