const express = require('express');
const router = express.Router();
const models = require('../../models');

router.get('/home', function (req, res, next) {
    res.render('user_home', {title: 'Express', user: req.user});
});

router.get('/restaurants', function (req, res) {
    models.restaurant.findAll().then(restaurants => {
        res.render('restaurants', {restaurants: restaurants});
    });
});

router.get('/reservationList', function(req, res) {
    models.reservation.findAll().then(reservations => {
        res.send('reservations', {reservations: reservations});
    })
});

router.get('/contact', function (req, res) {
        res.render('contact', {contact: contact});
});

module.exports = router;